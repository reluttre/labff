import serial
import time
import timeout_def
from inputimeout import inputimeout, TimeoutOccurred
import numpy as np
import matplotlib.pyplot as plt

TIMEOUT_S = timeout_def.TIMEOUT_S

def flush_buffer(ser_obj):
    while ser.in_waiting != 0:
        ser.read_line()

def get_input(prompt, timeout_s):
    try:
        ch = ord(inputimeout(prompt=prompt, timeout=timeout_s))
    except:
        ch = -1

    return ch

def wait_for_data():
    time.sleep(2)

def read_data():
    d = []
    while ser.in_waiting != 0:
        t_y_raw_str = ser.readline().decode("ascii")
        print(t_y_raw_str)
        t_y_arr_flt = [float(x.strip()) for x in t_y_raw_str.split(',')]
        d.append(t_y_arr_flt)

    return np.array(d)

if __name__ == "__main__":
    state = 0
    timeout_s = None

    while True:
        if state == 0:
            # Initialize serial comm
            ser = serial.Serial(port="/dev/ttyACM0", baudrate=115273, timeout=1)
            flush_buffer(ser)
            state = 1

        elif state == 1:
            # Get command to send to Nucleo
            ch = get_input("Enter a command: ", timeout_s)
            state = 2
            
            if ch == -1:
                # Data collection timeout
                state = 3
            
        elif state == 2:
            # Send command to Nucleo
            ser.write(chr(ch).encode("ascii"))
            
            if ch == ord("g"):
                timeout_s = TIMEOUT_S
                state = 1

            elif ch == ord("s"):
                state = 3

        elif state == 3:
            # Read data from Nucleo
            wait_for_data()
            d = read_data()
            state = 4

        elif state == 4:
            # Close serial comm
            ser.close()
            state = 5

        elif state == 5:
            # Process data
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.set_xlabel("y(t)")
            ax.set_ylabel("Time t [s]")
            ax.plot(*d.T)
            plt.show()
            np.savetxt("data.csv", d, fmt="%10.5f", delimiter=',')
            break



