"""
@file backend_week1.py
@brief A class serving as the backend task to simulate data collection using a function y(t).
@details Receives commands from the frontend and stores calculated data in an array to later transfer to the frontend.
Source code can be found at https://bitbucket.org/reluttre/labff/src/master

@author Robert Luttrell
@data 03/18/2021
"""
from pyb import UART
import utime
from math import *
import timeout_def

class Backend_Task:

    def __init__(self):
        """
        @brief constructor for the task
        """
        self.TIMEOUT_S = timeout_def.TIMEOUT_S  # Time to run data collection
        self.myuart = UART(2)  # uart object for comm with frontend
        self.state = -1

    def try_get_cmd(self):
        """
        @brief Gets a command from the frontend
        @details Returns new command if in buffer, else None.
        """
        if self.myuart.any() != 0:
            val = self.myuart.readchar()
            return val
        else:
            return None

    def process_cmd(self, cmd):
        """
        @brief returns a corresponding state based on the given command.
        @param cmd character representing command
        """
        if cmd == ord("g"):
            return 0
        elif cmd == ord("s"):
            return 2
        else:
            return self.state

    def transmit_arr(self):
        """
        @brief Transmits the data array to the frontend using the uart object
        """
        for elem in self.data:
            self.myuart.write(str(elem).encode())

    def y_func(self, t):
        """
        @brief Generates the value y(t) given t
        @param t time in seconds
        """
        val = exp(-t/10)*sin(2*pi/3*t)
        return val

    def run(self):
        """
        @brief main function that handles each state
        """
        self.cmd = self.try_get_cmd()
        self.state = self.process_cmd(self.cmd)

        if self.state == -1:
            # wait for input
            pass

        if self.state == 0:
            # Start/reset counter
            self.data = []
            self.ticks_t0 = utime.ticks_ms()
            self.state = 1

        elif self.state == 1:
            # Add data to array
            ticks_t = utime.ticks_ms()
            t_ms = utime.ticks_diff(ticks_t, self.ticks_t0)
            t_s = t_ms / 1000
            y = self.y_func(t_s)
            out_string = '{:}, {:}\r\n'.format(t_s, y)
            self.data.append(out_string)
            if abs(t_s) > self.TIMEOUT_S:
                self.state = 2
            utime.sleep(0.13)

        elif self.state == 2:
            # Send data to computer
            self.transmit_arr()
            self.data = []
