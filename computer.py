import serial
import time
from inputimeout import inputimeout, TimeoutOccurred

TIMEOUT_S = 15

def flush_buffer(ser_obj):
    while ser.in_waiting != 0:
        ser.read_line()

def get_input(prompt, timeout_s):
    try:
        ch = ord(inputimeout(prompt=prompt, timeout=timeout_s))
    except TimeoutOccurred:
        ch = -1

    return ch

ser = serial.Serial(port="/dev/ttyACM0", baudrate=115273, timeout=1)

flush_buffer(ser)

def wait_for_data():
    time.sleep(1)

def read_data():
    while ser.in_waiting != 0:
        print(ser.readline().decode("ascii"))

while True:
    ch = get_input("Enter a command: ", TIMEOUT_S)


    if ch == -1:
        break

    ser.write(chr(ch).encode("ascii"))

    if ch == ord("g"):
        timeout = TIMEOUT_S

    elif ch == ord("s"):
        break

wait_for_data()
read_data()

ser.close()
