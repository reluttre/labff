"""
@file frontend_week1.py
@brief The frontend for the week 1 portion of data collection
@details Acts as the user interface for the program.
Source code can be found at https://bitbucket.org/reluttre/lab3/src/master

@author Robert Luttrell
@data 02/14/2021
"""
import serial
import time
import timeout_def
from inputimeout import inputimeout, TimeoutOccurred
import numpy as np
import matplotlib.pyplot as plt

TIMEOUT_S = timeout_def.TIMEOUT_S

def flush_buffer(ser_obj):
    """
    @brief flushes the buffer
    @param ser_obj the serial object to be flushed
    """
    while ser.in_waiting != 0:
        ser.read_line()

def get_input(prompt, timeout_s):
    """
    @brief gets input from the user
    @param timeout_s time limit before terminating input request
    """
    try:
        ch = ord(inputimeout(prompt=prompt, timeout=timeout_s))
    except:
        ch = -1

    return ch

def wait_for_data():
    """
    @brief function to add a delay to ensure the data makes it to the buffer
    """
    time.sleep(2)

def read_data():
    """
    @brief reads data from the serial object
    """
    d = []
    while ser.in_waiting != 0:
        t_y_raw_str = ser.readline().decode("ascii")
        print(t_y_raw_str)
        t_y_arr_flt = [float(x.strip()) for x in t_y_raw_str.split(',')]
        d.append(t_y_arr_flt)

    return np.array(d)

if __name__ == "__main__":
    state = 0
    timeout_s = None

    while True:
        if state == 0:
            # Initialize serial comm
            ser = serial.Serial(port="/dev/ttyACM0", baudrate=115273, timeout=1)
            flush_buffer(ser)
            state = 1

        elif state == 1:
            # Get command to send to Nucleo
            ch = get_input("Enter a command: ", timeout_s)
            state = 2
            
            if ch == -1:
                # Data collection timeout
                state = 3
            
        elif state == 2:
            # Send command to Nucleo
            ser.write(chr(ch).encode("ascii"))
            
            if ch == ord("g"):
                timeout_s = TIMEOUT_S
                state = 1

            elif ch == ord("s"):
                state = 3

        elif state == 3:
            # Read data from Nucleo
            wait_for_data()
            d = read_data()
            state = 4

        elif state == 4:
            # Close serial comm
            ser.close()
            state = 5

        elif state == 5:
            # Process data
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.set_xlabel("y(t)")
            ax.set_ylabel("Time t [s]")
            ax.plot(*d.T)
            plt.show()
            np.savetxt("data.csv", d, fmt="%10.5f", delimiter=',')
            break



