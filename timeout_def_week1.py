"""
@file timeout_def_week1.py
@brief A file acting as a header file holding the timeout to be used by the backend and frontend

@author Robert Luttrell
@date 03/18/2021
"""
TIMEOUT_S = 30
