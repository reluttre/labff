"""
@file main_week1.py
@brief the main file running on the microcontroller
@details Runs the backend task in a loop

@author Robert Luttrell
@date 03/18/2021
"""
from backend import Backend_Task

if __name__ == "__main__":
    backend_task_1 = Backend_Task()

    while True:
        backend_task_1.run()
